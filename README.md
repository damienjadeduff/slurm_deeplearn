Using Keras/TF on a SLURM Cluster
=====================

Below's instructions are broken up according to which cluster the instructions are for:

* [ULAKBİM/TRUBA Instructions](#markdown-header-ulakbimtruba-instructions)
* [UHEM Instructions](#markdown-header-uhem-instructions)

ULAKBİM/TRUBA Instructions
=====================


Access login node
---------------------------------------------------------------------------

The Barbun cluster is the ones that has GPUs but as of the time of writing you access them via particular queues on the levrek1 login node.

You need to either individual or project access from ULAKBİM/TRUBA. In order to get this access you need to get:

1. a login/account to access the servers
2. a certificate for the VPN.

Instructions for accessing the VPN are here (in Turkish):

* http://wiki.truba.gov.tr/index.php/OpenVPN%27i_Nas%C4%B1l_Kullanabilirim%3F

Now you can access the login node. Run the VPN and then:

    ssh USERNAME@levrek1.ulakbim.gov.tr

**Note:** the password for this SSH account is different from that used for the VPN (whose password is built in to the certificate... you still need the password).

Now you are in the login node. From here you can submit jobs to the rest of the cluster.

This diagram from UHEM shows the situation nicely:

* http://wiki.uhem.itu.edu.tr/w/images/thumb/d/d9/Kume.gif/900px-Kume.gif

Setup your deep learning environment (pytorch)
---------------------------------------------------------------------------

**If you are using Keras or Tensorflow, skip to the next section**

While logged in to the login node (the file system is shared with all nodes - computers - on the cluster):

Fist checkout the code in the current repository:

    git clone git@bitbucket.org:damienjadeduff/slurm_deeplearn.git
    cd slurm_deeplearn

I have separated out the code into multiple folders. For the barbun cluster (and for Python 3) you will need:

    cd barbun_python3_pytorch

To set-up run ``setup.sh`` (do take the time to examine the script too... it is interactive... you will need to accept some licenses etc.).

    source setup.sh

It is quite simple - take a look at the code.

Setup your deep learning environment (tensorflow, keras)
---------------------------------------------------------------------------

**If you are using PyTorch, skip to the next section**

While logged in to the login node (the file system is shared with all nodes - computers - on the cluster):

Fist checkout the code in the current repository:

    git clone git@bitbucket.org:damienjadeduff/slurm_deeplearn.git
    cd slurm_deeplearn

I have separated out the code into multiple folders. For the barbun cluster (and for Python 3) you will need:

    cd barbun_python3

To set-up run ``setup.sh`` (do take the time to examine the script too... it is interactive... you will need to accept some licenses etc.).

    source setup.sh

It makes use of the TRUBA-supplied CUDA installation, installs CUDNN in your home folder, installs bazel and uses it to build tensorflow. It has to do some tricks with linking of libraries and library paths. It then installs Miniconda, tensorflow, keras and some other packages (PyOpenCV etc.). To use them you have to set up some environment variables but the test scripts I supply (discussed below) show how to do this. Most of the things installed are done into your Conda ( https://conda.io/docs/ ) environment.

Submit deep learning jobs to a computer with a GPU
---------------------------------------------------------------------------

But the login node has no GPU!

So we need to submit a job to the cluster (on the job queue ``barbunya-cuda`` which contains the computers with GPUs) using SLURM.

For more information you can go to:

* http://wiki.truba.gov.tr/index.php/Kullan%C4%B1c%C4%B1_El_Kitab%C4%B1#Kaynak_Y.C3.B6neticisi_ve_.C4.B0.C5.9F_D.C3.B6ng.C3.BCs.C3.BC

But here is the basic approach, with examples specially for deep learning:

First of all let's run a script (you downloaded it with this repository) to see the CUDA GPU capabilities are working (note that Keras is built on tensorflow which is built on CUDA so it makes sense to test first CUDA then tensorflow then Keras):

    sbatch test_cuda.sh

Investigate the script to see how it works - it is a SLURM batch job.

You should be able see the output  in a file called ``slurm-NNNN.out`` where ``NNNN`` was the job number.

Other useful SLURM commands include:

    squeue
    scancel

There is more info about that on the wiki linked above.

Next, let's run a script (you downloaded it with this repository) to run a simple tensorflow program:

    sbatch test_dl_gpu.sh

Again, you can see the output in a ``slurm-NNNN.out`` file. Check what that script is doing too - you should be starting to get the idea.

Now let's test your chosen environment (Keras or PyTorch) with training on the MNIST dataset. Examine the batch job script ``test_mnist.sh`` and run it:

    sbatch test_mnist.sh

You should see in the output file the result of some training, and if not already trained, the weights and architecture files should appear in the current directory.

Yay!

For more information about SLURM, see:

* http://wiki.truba.gov.tr/index.php/TRUBA-barbun#Ortam_ve_Programlama


UHEM Instructions
=====================


Access SARIYER login node
-------------------------


The Sariyer cluster is the ones that has GPUs. You need to request access to one of them from UHEM. With the Sarıyer cluster, each job you submit claims a minimum of 28 cores with no extra cost for the GPU.

First of all, you need to activate the VPN to UHEM. If you are on windows the instructions are on the wiki (you will need your activation email from UHEM):

* http://wiki.uhem.itu.edu.tr/w/index.php/VPN_Kullan%C4%B1m_K%C4%B1lavuzu

For ubuntu and derivatives you install the relevant VPN software like this:

In a terminal:

    sudo apt-get install network-manager-vpnc network-manager-vpnc-gnome

Then using GUI:

* Add a new connection "Cisco Compatible VPN (vpnc)".
* Fill in the gateway/host, username, group name, and group password.
* Connect.

Now you need to ssh into the login node (with the VPN active). 
    
For the Sariyer cluster:

    ssh USERNAME@sariyer.uhem.itu.edu.tr
    
**Note:** the password for this ssh account is different from that used for the VPN. In total you have 3 passwords: 2 for the VPN (user and group) and one for the SSH server.

Now you are in the login node. From here you can submit jobs to the rest of the cluster.

This diagram from UHEM shows the situation nicely:

* http://wiki.uhem.itu.edu.tr/w/images/thumb/d/d9/Kume.gif/900px-Kume.gif

Setup your deep learning environment (tensorflow, keras)
---------------------------------------------------------------------------

While logged in to the login node (the file system is shared with all nodes - computers - on the cluster):

Fist checkout the code in the current repository:

    git clone https://damienjadeduff@bitbucket.org/damienjadeduff/slurm_deeplearn.git
    cd slurm_deeplearn

I have separated out the code into two folders: one for Python 2 on ``sariyer`` and one for Python 3 on ``sariyer``. CD in to the appropriate folder to run these tests. E.g.:

    cd sariyer_python3_tfkeras

**Warning**: The Python 2 version has not been tested for several months and may be out of date.
    
To set-up run ``setup.sh`` (do take the time to examine the script too).

    source setup.sh
    
It uses the SLURM-enabled pre-installed Anaconda and any packages installed into the Anaconda environment you create are installed into your home directory so whenever you need to use the stuff that setup.sh installed, you need load the SLURM Anaconda module and activate the conda environment (called ``deep``) that we set-up on the login node like this:

    module load Anaconda/Anaconda3-5.0.1-Cuda-8.0
    source activate deep
    
Submit deep learning jobs to a computer with a GPU 
---------------------------------------------------------------------------

But the login node has no GPU!

So we need to submit a job to the cluster (on the job queue ``gpuq`` which contains the computers with GPUs) using SLURM.

For more information you can go to:

* http://wiki.uhem.itu.edu.tr/w/index.php/Sar%C4%B1yer_sistemine_i%C5%9F_vermek

But here is the basic approach, with examples specially for deep learning:

First of all let's run a script (you downloaded it with this repository) to see the tensorflow GPU capabilities are working:

    sbatch test_dl_gpu.sh
    
Investigate the script to see how it works - it is a SLURM batch job. 

You should be able see the output  in a file called ``slurm-NNNN.out`` where ``NNNN`` was the job number.

If this file does not turn up immediately it may be because your job is waiting in a queue. Use the ``squeue`` command to see its status.

Other useful SLURM commands include:

    sinfo # informations about the partitions/queues on the cluster
    scancel # cancel your job
    module avail # learn about other SLURM modules that are available

There is more info about that on the wiki linked above.
    
Now let's test keras with mnist. Examine the batch job script ``test_mnist.sh`` and run it:

    sbatch test_mnist.sh
    
You should see in the output file the result of some training, and if not already trained, the weights and architecture files should appear in the current directory.

Yay!


