# borrowed from https://gist.github.com/xmfbit/b27cdbff68870418bdb8cefa86a2d558
import torch
from torchvision import datasets, transforms
import os, os.path
import torch.nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy.random
device = torch.device("cuda")
#device = torch.device("cpu")
print("Acquiring dataset")
datasetroot = os.path.expanduser('~')+'/tmp/pytorch_mnist_dataset'
try:
    os.mkdirs(datasetroot)
except:
    pass

transforms = transforms.Compose([
        transforms.ToTensor(), 
        transforms.Normalize((0.5,), (1.0,))]
    )

MNIST_train = datasets.MNIST(datasetroot,train=True,download=True,transform=transforms)
MNIST_test = datasets.MNIST(datasetroot,train=False,download=True,transform=transforms)

train_loader = torch.utils.data.DataLoader(
                 dataset=MNIST_train    ,
                 batch_size=32,shuffle=True)

test_loader = torch.utils.data.DataLoader(
                 dataset=MNIST_test,
                 batch_size=32,shuffle=True)

print("Building model")

class LeNet(torch.nn.Module):
    def __init__(self):
        super(LeNet, self).__init__()
        self.conv1 = torch.nn.Conv2d(1, 20, 5, 1)
        self.conv2 = torch.nn.Conv2d(20, 50, 5, 1)
        self.fc1 = torch.nn.Linear(4*4*50, 500)
        self.fc2 = torch.nn.Linear(500, 10)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2, 2)
        x = x.view(-1, 4*4*50)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x
    
    def name(self):
        return "LeNet"
    
model = LeNet().to(device)

try:

    print("Trying to load saved")
    saved = torch.load(model.name())
    print("Trying to initialise model with previous training")
    model.load_state_dict(saved)
    print("Loaded.")
except:
    print("No previously saved model - better train.")
    optimizer = torch.optim.SGD(model.parameters(), lr=0.01, momentum=0.9)
    
    loss_func = torch.nn.CrossEntropyLoss()
    
    for epoch in range(2):
        print("Epoch",epoch)
        # trainning
        ave_loss = 0
        for batch_idx, (x, target) in enumerate(train_loader):
            optimizer.zero_grad()
            x, target = x.to(device), target.to(device)
            x, target = Variable(x), Variable(target)
            out = model(x)
            loss = loss_func(out, target)
            ave_loss = ave_loss * 0.9 + loss.data[0] * 0.1
            loss.backward()
            optimizer.step()
            if (batch_idx+1) % 100 == 0 or (batch_idx+1) == len(train_loader):
                print('==>>> epoch: {}, batch index: {}, train loss: {:.6f}'.format(
                    epoch, batch_idx+1, ave_loss))
        
        torch.save(model.state_dict(), model.name())
# testing
correct_cnt, ave_loss = 0, 0
total_cnt = 0
print("Now testing")
for batch_idx, (x, target) in enumerate(test_loader):
    x, target = x.to(device), target.to(device)
    x, target = Variable(x, volatile=True), Variable(target, volatile=True)
    out = model(x)
    loss = loss_func(out, target)
    _, pred_label = torch.max(out.data, 1)
    total_cnt += x.data.size()[0]
    correct_cnt += (pred_label == target.data).sum()
    # smooth average
    ave_loss = ave_loss * 0.9 + loss.data[0] * 0.1
    
    if(batch_idx+1) % 100 == 0 or (batch_idx+1) == len(test_loader):
        print('==>>> epoch: {}, batch index: {}, test loss: {:.6f}, acc: {:.3f}'.format(
            epoch, batch_idx+1, ave_loss, correct_cnt * 1.0 / total_cnt))



