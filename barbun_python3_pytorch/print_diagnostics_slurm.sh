#!/bin/bash

echo
echo -----------------------------
echo DIAGNOSTICS
echo -----------------------------
echo
echo I print some diagnostic info to help track down any problems in the cluster job.
echo
echo What is my hostname?
hostname
echo
echo What Python jobs are currently running?
echo
ps aux| grep python
echo
echo What is the status of nvidia-smi?
echo
nvidia-smi
echo
echo -----------------------------
echo Diagnostics finished
echo -----------------------------
echo