#!/bin/bash

export NUMJOBS=4 # let's not wake the sleeping beast

echo THIS SCRIPT MAY NEED YOUR ATTENTION
echo
echo YOU MAY USE GNU SCREEN TO ALLOW IT TO CONTINUE IF YOU GET DISCONNECTED
echo
echo BUT THIS SCRIPT IS QUITE SIMPLE BECAUSE PYTORCH IS EASY TO INSTALL
echo
echo GNU SCREEN CAN BE RUN BY USING
echo screen
echo
echo AND REATTACHED WITH
echo screen -r
echo
echo AND REATTACHED IF THE PREVIOUS ONE DID NOT DETACH RIGHT E.G. A NETWORK PROBLEM
echo screen -x
echo
echo PRESS ENTER TO CONTINUE
read garbage

module avail

echo ABOVE A LIST OF AVAILABLE MODULES

echo
echo --------------------- INSTALL MINICONDA AND MAKE ENV -------------------------
echo
sleep 1

mkdir -p $HOME/tmp
cd $HOME/tmp || { echo "Failed on line $LINENO" ; exit 1; }

wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh

bash Miniconda3-latest-Linux-x86_64.sh -b

export PATH="$HOME/miniconda3/bin:$PATH"

yes | conda remove --name tempenv --all
yes | conda create --name tempenv --clone pytorch
yes | conda remove --name pytorch --all
yes | conda create --name pytorch
# yes | conda remove --name tempenv --all

source activate pytorch || { echo "Failed on line $LINENO" ; exit 1; }

echo
echo --------------------- INSTALL SOME CONDA PACKAGES -------------------------
echo
# sleep 1

source activate pytorch
# yes | conda install numpy six wheel


echo
echo --------------------- INSTALL PYTORCH -------------------------
echo
sleep 1

source activate pytorch
yes | conda install pytorch torchvision -c pytorch

echo
echo --------------------- done -------------------------
echo
echo TO TEST, run:
echo sbatch test_pytorch_gpu.sh
echo sbatch test_pytorch_mnist.sh

