#!/bin/bash
####SBATCH -A XXXXXX            # account / proje adi
#SBATCH -N 1                 # bilgisayar sayisi
#SBATCH -n 4                    # cekirdek / islemci sayisi
#SBATCH -p barbun-cuda          # kuyruk (partition/queue) adi
#SBATCH --gres=gpu:1            # ilave kaynak (1 gpu gerekli)

bash print_diagnostics_slurm.sh > slurm_extra_diagnostics_`date +"%s%N"`.txt

echo This script does nothing because PyTorch installs its own copy of CUDA. So there is no point testing cuda without testing PyTorch.
echo But we will show anyway the output of nvidia-smi to ensure there are some GPUs.

echo
echo xxxxxxxxxxxxxxxxxxxxxxxxx nvidia-smi
echo

nvidia-smi
