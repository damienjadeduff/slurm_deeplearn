#!/bin/bash
#####SBATCH -A XXXX               # account / proje adi
#SBATCH -N 1                    # bilgisayar sayisi
#SBATCH -n 4                    # cekirdek / islemci sayisi
#SBATCH -p barbun-cuda                # kuyruk (partition/queue) adi
#SBATCH --gres=gpu:1            # ilave kaynak (1 gpu gerekli)

bash print_diagnostics_slurm.sh > slurm_extra_diagnostics_`date +"%s%N"`.txt

export PATH="$HOME/miniconda3/bin:$PATH"

source activate pytorch

echo
echo ---------- Here is the output of nvidia-smi ----------------
echo

nvidia-smi

echo
echo ----------  Now Running test ----------
echo

python test_pytorch_gpu.py

echo
echo "Ran test"
echo