#!/bin/bash
#####SBATCH -A XXXX               # account / proje adi
#SBATCH -N 1                    # bilgisayar sayisi
#SBATCH -n 4                    # cekirdek / islemci sayisi
#SBATCH -p barbun-cuda                 # kuyruk (partition/queue) adi
#SBATCH --gres=gpu:1            # ilave kaynak (1 gpu gerekli)

bash print_diagnostics_slurm.sh > slurm_extra_diagnostics_`date +"%s%N"`.txt

export PATH="$HOME/miniconda3/bin:$PATH"

source activate pytorch

echo
echo First print nvidia-smi
echo

nvidia-smi

echo
echo now run demo
echo

python mnist_demo_headless_pytorch.py

