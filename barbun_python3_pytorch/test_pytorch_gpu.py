# -*- coding: utf-8 -*-
# from https://pytorch.org/tutorials/beginner/pytorch_with_examples.html#tensors
import torch
from time import time

dtype = torch.float

print('Number of GPUs:',torch.cuda.device_count())
print('GPU name:',torch.cuda.get_device_name(0))

def task():

    N, D_in, H, D_out = 64, 1000, 100, 10

    x = torch.randn(N, D_in, device=device, dtype=dtype)
    y = torch.sin(5+x).mm(torch.ones([D_in,D_out],device=device,dtype=dtype))

    w1 = torch.randn(D_in, H, device=device, dtype=dtype, requires_grad=True)
    w2 = torch.randn(H, D_out, device=device, dtype=dtype, requires_grad=True)

    #print('Current device:',torch.cuda.current_device())
    print('Used device:',device)

    learning_rate = 1e-6
    time1 = time()
    for t in range(500):

        y_pred = x.mm(w1).clamp(min=0).mm(w2)

        loss = (y_pred - y).pow(2).sum()
        if t % 200 == 0 or t == 499:
            print("iter:",t, "loss:",loss.item())

        loss.backward()

        # Manually update weights using gradient descent. Wrap in torch.no_grad()
        # because weights have requires_grad=True, but we don't need to track this
        # in autograd.
        # An alternative way is to operate on weight.data and weight.grad.data.
        # Recall that tensor.data gives a tensor that shares the storage with
        # tensor, but doesn't track history.
        # You can also use torch.optim.SGD to achieve this.
        with torch.no_grad():
            w1 -= learning_rate * w1.grad
            w2 -= learning_rate * w2.grad

            w1.grad.zero_()
            w2.grad.zero_()

    time2 = time()
    print("Seconds taken to do this simple task:",time2-time1)

print("checking with GPU")
device = torch.device("cuda:0")
task()
print("checking with CPU")
device = torch.device("cpu")
task()
