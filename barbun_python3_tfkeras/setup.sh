#!/bin/bash

export NUMJOBS=4 # let's not wake the sleeping beast

CUDNN_TAR_FILE="cudnn-9.0-linux-x64-v7.tgz"
CUDNN_ADDR="http://developer.download.nvidia.com/compute/redist/cudnn/v7.0.5/${CUDNN_TAR_FILE}"

echo THIS SCRIPT WILL NEED YOUR ATTENTION
echo
echo I SUGGEST YOU USE GNU SCREEN TO ALLOW IT TO CONTINUE IF YOU GET DISCONNECTED
echo
echo GNU SCREEN CAN BE RUN BY USING
echo screen
echo
echo AND REATTACHED WITH
echo screen -r
echo
echo AND REATTACHED IF THE PREVIOUS ONE DID NOT DETACH RIGHT (E.G. A NETWORK PROBLEM)
echo screen -x
echo
echo PRESS ENTER TO CONTINUE
read garbage

module avail

echo ABOVE A LIST OF AVAILABLE MODULES

mkdir -p $HOME/tmp
cd $HOME/tmp || { echo "Failed on line $LINENO" ; exit 1; }


echo
echo --------------------- PROVIDE CUDA -------------------------
echo
sleep 1

module load centos7.3/lib/cuda/9.0

mkdir -p $HOME/software/cuda9.0/lib64/stubs/
cp $CUDA_TOOLKIT_PATH/lib64/stubs/libcuda.so $HOME/software/cuda9.0/lib64/stubs/libcuda.so.1
# above 2 lines necessary to compile tensorflow but the file should be removed after it is compiled - see bottom of this script

export LD_LIBRARY_PATH=$HOME/software/cudnn/cuda/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$HOME/software/cuda9.0/lib64/stubs/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$CUDA_TOOLKIT_PATH/lib64/stubs:$LD_LIBRARY_PATH


echo
echo --------------------- INSTALL CUDNN -------------------------
echo
sleep 1

mdir -p $HOME/software/cudnn
cd $HOME/software/cudnn || { echo "Failed on line $LINENO" ; exit 1; }
wget $CUDNN_ADDR
tar -xzvf $CUDNN_TAR_FILE


echo
echo --------------------- INSTALL MINICONDA AND MAKE ENV -------------------------
echo
sleep 1

mkdir -p $HOME/tmp
cd $HOME/tmp || { echo "Failed on line $LINENO" ; exit 1; }

wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh

bash Miniconda3-latest-Linux-x86_64.sh -b

export PATH="$HOME/miniconda3/bin:$PATH"

yes | conda remove --name tempenv --all
yes | conda create --name tempenv --clone deep
yes | conda remove --name deep --all
yes | conda create --name deep
yes | conda remove --name tempenv --all

source activate deep || { echo "Failed on line $LINENO" ; exit 1; }


echo
echo --------------------- INSTALL BAZEL -------------------------
echo
sleep 1

mkdir $HOME/software/bazel

cd $HOME/software/bazel ||  { echo "Failed on line $LINENO" ; exit 1; }

wget https://github.com/bazelbuild/bazel/releases/download/0.10.0/bazel-0.10.0-dist.zip

unzip bazel-0.10.0-dist.zip
mv bazel-0.10.0-dist.zip $HOME/deleteme_please_my_usefulness_is_past.zip

./compile.sh


echo
echo --------------------- INSTALL SOME CONDA PACKAGES -------------------------
echo

source activate deep
yes | conda install numpy six wheel


echo
echo --------------------- GET AND CONFIGURE TENSORFLOW -------------------------
echo
sleep 1

cd $HOME/software || exit 1

rm -r bak_tensorflow
mv tensorflow bak_tensorflow

git clone https://github.com/tensorflow/tensorflow

cd tensorflow || { echo "Failed on line $LINENO" ; exit 1; }

git pull
git checkout v1.6.0
# git checkout master

bazel clean

# export PYTHON_PATH=$HOME/miniconda3/
export PYTHON_BIN_PATH="$HOME/miniconda3/envs/deep/bin/python"
export PYTHON_LIB_PATH="$HOME/miniconda3/envs/deep/lib/python3.6/site-packages"
export TF_NEED_JEMALLOC="0"
export TF_NEED_GCP="0"
export TF_NEED_HDFS="0"
export TF_ENABLE_XLA="0"
export TF_NEED_VERBS="0"
export TF_NEED_OPENCL="0"
export TF_NEED_CUDA="1"
export TF_CUDA_CLANG="0"
export TF_CUDA_VERSION="9.0"
export CUDA_TOOLKIT_PATH="$CUDA_DIR"
#     export CUDA_TOOLKIT_PATH="/usr/local/cuda"
export GCC_HOST_COMPILER_PATH="/usr/bin/gcc"
export TF_CUDNN_VERSION="7"
export CUDNN_INSTALL_PATH="$HOME/software/cudnn/cuda"
export TF_CUDA_COMPUTE_CAPABILITIES="5.2,6.0,6.1"
export TF_NEED_MPI="0"
export TF_NEED_GDR="0"
export TF_NEED_S3="0"
export TF_NEED_OPENCL="0"

export PATH=$HOME/software/bazel/output:$PATH

echo IT SHOULD BE POSSIBLE TO ACCEPT THE DEFAULTS FROM THE FOLLOWING QUESTIONS

python configure.py

echo ENV
env


echo
echo --------------------- BUILD TENSORFLOW - THIS CAN TAKE A WHILE -------------------------
echo
sleep 1

echo NOW WE ARE GOING TO BUILD... THIS WILL ONLY FINISH WHEN THE BUILD FINISHES SUCCESSFULLY OR FAILS
echo YOU CAN FIND THE OUTPUT IN THE FILES builderr.txt and buildout.txt as this continues - I prefer not to spit all this onto a terminal
echo building tensorflow...

bazel build --jobs $NUMJOBS --config=mkl --config=opt --config=cuda //tensorflow/tools/pip_package:build_pip_package --action_env=LD_LIBRARY_PATH=$LD_LIBRARY_PATH 2> builderr.txt > buildout.txt


echo
echo BUILT. NOW TO INSTALL.
echo

echo
echo --------------------- INSTALL TENSORFLOW -------------------------
echo
sleep 1

mkdir -p $HOME/tmp

rm -r $HOME/tmp/tensorflow_pkg/

bazel-bin/tensorflow/tools/pip_package/build_pip_package $HOME/tmp/tensorflow_pkg

yes | conda upgrade --all
yes | pip install --upgrade pip
yes | pip uninstall -y tensorflow
yes | pip install --upgrade  $HOME/tmp/tensorflow_pkg/tensorflow*.whl

# cd
# python -c 'import tensorflow as tf;hello = tf.constant("Hello, TensorFlow!");sess = tf.Session();print(sess.run(hello))'


echo
echo --------------------- REMOVING LIBCUDA.SO.1 STUB -------------------------
echo
sleep 1

rm $HOME/software/cuda9.0/lib64/stubs/libcuda.so.1


echo
echo --------------------- NOW TO INSTALL KERAS - ALSO FROM SOURCE -------------------------
echo
sleep 1

yes | conda install h5py

mkdir -p $HOME/software/
cd $HOME/software/ || { echo "Failed on line $LINENO" ; exit 1; }
git clone https://github.com/keras-team/keras.git
# cd keras || { echo "Failed on line $LINENO" ; exit 1; }
pip install -e $HOME/software/keras

echo finished

echo TO TEST, run:
echo sbatch test_tf_gpu.sh
echo sbatch test_mnist.sh

