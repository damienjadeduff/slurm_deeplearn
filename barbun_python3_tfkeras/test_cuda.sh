#!/bin/bash
####SBATCH -A XXXXXX            # account / proje adi
####SBATCH -N 0                 # bilgisayar sayisi
#SBATCH -n 4                    # cekirdek / islemci sayisi
#SBATCH -p barbun-cuda          # kuyruk (partition/queue) adi
#SBATCH --gres=gpu:1            # ilave kaynak (1 gpu gerekli)

module load centos7.3/lib/cuda/9.0
# module load centos7.3/app/gromacs/5.1.4-openmpi-1.8.8-gcc-4.8.5-E5V3-cuda
# module load centos7.3/app/namd/2017-11-10-multicore-cuda
# module load centos7.3/app/namd/2.9-multicore-cuda
# module avail

echo
echo xxxxxxxxxxxxxxxxxxxxxxxxx nvidia-smi
echo

nvidia-smi

echo
echo xxxxxxxxxxxxxxxxxxxxxxxxx deviceQuery precompiled
echo

$CUDA_DIR/extras/demo_suite/deviceQuery

echo
echo xxxxxxxxxxxxxxxxxxxxxxxxx deviceQuery I compile
echo

mkdir -p $HOME/tmp/testcuda

rm -r $HOME/tmp/testcuda/1_Utilities
rm -r $HOME/tmp/testcuda/common

cp -r $CUDA_DIR/NVIDIA_CUDA-9.0_Samples/1_Utilities/ $HOME/tmp/testcuda/
cp -r $CUDA_DIR/NVIDIA_CUDA-9.0_Samples/common $HOME/tmp/testcuda/

cd $HOME/tmp/testcuda/1_Utilities/deviceQuery

echo
echo xxxxxxxxxxxxxxx make
echo
make

echo
echo xxxxxxxxxxxxxxx run
echo
./deviceQuery

