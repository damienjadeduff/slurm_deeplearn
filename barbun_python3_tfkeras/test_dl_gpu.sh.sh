#!/bin/bash
####SBATCH -A XXXXXX               # account / proje adi
#SBATCH -N 1                    # bilgisayar sayisi
#SBATCH -n 4                    # cekirdek / islemci sayisi
#SBATCH -p barbun-cuda                # kuyruk (partition/queue) adi
#SBATCH --gres=gpu:1            # ilave kaynak (1 gpu gerekli)

module load centos7.3/lib/cuda/9.0

export PATH="$HOME/miniconda3/bin:$PATH"

export LD_LIBRARY_PATH=$HOME/software/cudnn/cuda/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$HOME/software/cuda9.0/lib64/stubs/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$CUDA_TOOLKIT_PATH/lib64/stubs:$LD_LIBRARY_PATH

source activate deep

nvidia-smi

python test_tf_gpu.py
