#!/bin/bash

module avail

#     DONT USE THIS: module load miniconda/miniconda-4.2.12-python3.5

#     DO USE THIS: module load cuda75/toolkit/7.5.18

module load cuda/cuda-8.0

mkdir -p ~/tmp
cd ~/tmp || { echo "Failed on line $LINENO" ; exit 1; }

wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh

bash Miniconda3-latest-Linux-x86_64.sh 

export PATH="$HOME/miniconda3/bin:$PATH"

conda create --name deep2

source activate deep2 || { echo "Failed on line $LINENO" ; exit 1; }

conda update --all

conda install theano pygpu

conda install -c https://conda.binstar.org/menpo opencv3

echo possibly opencv rather than opencv3 would be better here

conda install numpy scipy

conda install mkl nose matplotlib 

conda upgrade --all

pip install --upgrade pip 

conda install  -y tensorflow-gpu keras h5py
conda uninstall -y tensorflow-gpu cudatoolkit

pip install --upgrade tensorflow-gpu keras

pip uninstall setuptools

pip install --upgrade setuptools

pip install --upgrade tensorflow-gpu keras


#conda install -y  tensorflow-gpu

# conda install keras

#mkdir ~/software
#cd ~/software
#git clone https://github.com/fchollet/keras.git

#cd keras || { echo "Failed on line $LINENO" ; exit 1; }

#python setup.py install

#conda uniinstall -y tensorflow-gpu

#conda install  -y tensorflow-gpu

echo finished

echo TO TEST, run:
echo sbatch test_mnist.sh

