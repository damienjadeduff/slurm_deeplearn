#!/bin/bash
#SBATCH -A XXXXXX               # account / proje adi
#SBATCH -N 1                    # bilgisayar sayisi
#SBATCH -n 1                    # cekirdek / islemci sayisi
#SBATCH -p gpuq                 # kuyruk (partition/queue) adi
#SBATCH --gres=gpu:1            # ilave kaynak (1 gpu gerekli)

export PATH="$HOME/miniconda3/bin:$PATH"

module load cuda/cuda-8.0

source activate deep2

nvidia-smi

# export cdir=`pwd`

# cd ../deviceQuery

# make

# ./deviceQuery 

cd $cdir

python test_tf_gpu.py
