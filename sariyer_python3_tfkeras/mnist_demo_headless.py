
# based on tutorial here: https://elitedatascience.com/keras-tutorial-deep-learning-in-python

# but I have added some interesting visualisations and adapted it to the latest version of keras

# Also, does not train twice. After first training, saves weights and reloads them if run again.

# 3. Import libraries and modules
import numpy as np
import numpy.random as random
np.random.seed(123)  # for reproducibility
import h5py
from keras.models import Sequential,Model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras.datasets import mnist
from keras.callbacks import TensorBoard

#import keras.backend

#backend_str = keras.backend.backend()
#assert(backend_str=="tensorflow")

print("get data")

# 4. Load pre-shuffled MNIST data into train and test sets
(X_train, y_train), (X_test, y_test) = mnist.load_data()

print("preprocess")
 
# 5. Preprocess input data

X_train = X_train.reshape(X_train.shape[0], 28, 28, 1)
X_test = X_test.reshape(X_test.shape[0], 28, 28, 1)
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255

print("get labels") 

# 6. Preprocess class labels
Y_train = np_utils.to_categorical(y_train, 10)
Y_test = np_utils.to_categorical(y_test, 10)
 
print("build model")

# 7. Define model architecture
model = Sequential()
 
model.add(Conv2D(32, (3, 3), activation='relu', input_shape=(28,28,1),name="conv1",use_bias=False))
model.add(Conv2D(32, (3, 3), activation='relu', name="conv2",use_bias=False))
model.add(MaxPooling2D(pool_size=(2,2), name="mp1"))
model.add(Dropout(0.25))
 
model.add(Flatten())
model.add(Dense(128, activation='relu',name="dense1"))
model.add(Dropout(0.5))
model.add(Dense(10, activation='softmax',name="dense2"))

print("compile")

# 8. Compile model
model.compile(loss='categorical_crossentropy',
          optimizer='adam',
          metrics=['accuracy'])

 
try:
    model.load_weights('mnist_weights_trained.h5')
except:
 
   
    print("fit")
    
    # vis not quite working
    callbacks=[]
#    callbacks.append(
#            TensorBoard(
#                    log_dir='./logs', histogram_freq=0, write_graph=True, write_images=False
#                    )
#            )
#    callbacks[-1].set_model(model)

    
    # 9. Fit model on training data
    model.fit(X_train, Y_train, 
              batch_size=32, epochs=2, verbose=1,
              callbacks=callbacks)
    
    model.save_weights('mnist_weights_trained.h5')
    
    model.save("mnist_architecture_trained.h5")
     
print("evaluate")


intermediate_layer_model1 = Model(inputs=model.input,
                                 outputs=model.get_layer("conv1").output)

intermediate_layer_model2 = Model(inputs=model.input,
                                 outputs=model.get_layer("mp1").output)

num_datapoints = X_test.shape[0]

disp = random.randint(0,num_datapoints,4)

for ind in disp:
    print()
    print()
    print("ind",ind,"label:",y_test[ind])
    print(X_test[ind,:,:,0].shape)
    inp = X_test[ind,:,:,0].reshape((1,28,28,1))
    disp = X_test[ind,:,:,0]
#    imgplot = plt.imshow(disp,cmap="gray")
    
#    plt.show()
    intermediate_output1 = intermediate_layer_model1.predict(inp)
    intermediate_output2 = intermediate_layer_model2.predict(inp)
#    print(intermediate_output.shape)
    for channel in np.arange(0,32,3):
        
#        imgplot2 = plt.imshow(intermediate_output[0,0,:,:],cmap="gray")
        col1 = intermediate_output1[0,:,:,channel:channel+3]
#        col1 = np.moveaxis(col1,0,-1)
        if col1.shape[2]==2:
            continue
        col2 = intermediate_output2[0,:,:,channel:channel+3]
#        col2 = np.moveaxis(col2,0,-1)
        if col2.shape[2]==2:
            continue
#        col = np.moveaxis(intermediate_output[0,channel:channel+2,:,:],-1,0)
#        print(col1.shape)
#        print(col2.shape)
#        imgplot1 = plt.imshow(col1)
#        plt.show()
#        imgplot2 = plt.imshow(col2)
#        plt.show()

    print("prediction",model.predict(inp))
    print("predicted label",np.argmax(model.predict(inp)),"should be:",y_test[ind])
    print("------------------------")
    

# 10. Evaluate model on test data
score = model.evaluate(X_test, Y_test, verbose=0)

print("trained network score on test data - loss, accuracy",score)
