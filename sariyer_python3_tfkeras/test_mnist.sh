/bin/bash
#SBATCH -A XXXXXX               # account / proje adi
#SBATCH -N 1                    # bilgisayar sayisi
#SBATCH -n 1                    # cekirdek / islemci sayisi
#SBATCH -p gpuq                 # kuyruk (partition/queue) adi
#SBATCH --gres=gpu:1            # ilave kaynak (1 gpu gerekli)

module load Anaconda/Anaconda3-5.0.1-Cuda-8.0

source activate deep

nvidia-smi

python mnist_demo_headless.py

